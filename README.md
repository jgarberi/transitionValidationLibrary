# Advert state validator

Basic library to validate transitions between states, implementing the following graph of advert
states:

```
  +-------------------------------------------------------------------+
  |                                                                   |
  |                  +---------+                                      |
  |                  |         |                                      |
  |         +--------> Limited |                                      |
  |         |        |         |                                      |
  |         |        +---------+                                      |
  |         |             |                                           |
  |         |             |                                           |
  |         |             |                                           |
  |    +---------+   +----v----+   +---------+   +---------+          |
  |    |         |   |         |   |         |   |         |          |
  |    | Initial +---> Active  +---> Outdated+---> Removed |          |
  |    |         |   |         |   |         |   |         |          |
  |    +---------+   +---------+   +---------+   +---------+          |
  |                       ^             |                             |
  |                       |             |                             |
  |                       +-------------+                             |
  |                                                                   |
  |    INITIAL STATE +-------------------------->FINAL STATE          |
  |                                                                   |
  |                                                                   |
  |                                                                   |
  |                                                                   |
  +-------------------------------------------------------------------+
```
## Install

The best way to install transitionsValidator is [through composer](http://getcomposer.org).

Just create a composer.json file for your project:

```JSON
{
    "require": {
        "jgarberi/transitionsValidator": "~0.1"
    }
}
```

Then you can run these two commands to install it:

    $ curl -s http://getcomposer.org/installer | php
    $ php composer.phar install

or simply run `composer install` if you have have already [installed the composer globally](http://getcomposer.org/doc/00-intro.md#globally).

Then you can include the autoloader, and you will have access to the library classes:

```php
<?php

    require_once __DIR__ . '/vendor/autoload.php';
    
    // To ease the promotion actions, retrieve the enum class for all allowed states
    use transitionsValidator\States;

    // Create a new "creative" object, we provide one for you to test
    $creative  = new \transitionsValidator\Advert('Spring Break campaign', 'Coca Cola');
    
    // Create an advert to promote, and send the creative object you just created
    // In this version, we only operate over the states, but this creative object is accesible 
    // from within the advertContext to manipulate it
    $advert = new \transitionsValidator\advertContext($creative);

    // Promote the advert, keep in mind the flow stated above...
    $advert->promoteAdvert(States::Limited);


```
