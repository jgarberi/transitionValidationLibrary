<?php

namespace transitionsValidator;

class AdvertOutdatedState implements AdvertState {

    private $label = States::Outdated;

    public function promote(callable $ok, callable $error, string $action) {

        switch ($action) {
            case States::Active:
                $ok(new AdvertActiveState());
                break;
            case States::Removed:
                $ok(new AdvertRemovedState());
                break;
            default:
                $error("{$this->label} state only allows promotion to either " . States::Active . " or " . States::Removed);
                break;
        }
    }
    
    public function getLabel():string {
        return $this->label;
    }
}
