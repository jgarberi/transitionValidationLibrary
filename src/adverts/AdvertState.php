<?php

namespace transitionsValidator;

interface AdvertState {
 
    public function promote(callable $ok, callable $error, string $action);
    
    public function getLabel():string;
}
