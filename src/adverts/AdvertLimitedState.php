<?php

namespace transitionsValidator;

use transitionsValidator\States;

class AdvertLimitedState implements AdvertState {

    private $label = States::Limited;

    public function promote(callable $ok, callable $error, string $action) {

        switch ($action) {
            case States::Active:
                $ok(new AdvertActiveState());
                break;
            default:
                $error($this->label . " state only allows to promote to Active");
                $error("{$this->label} state only allows promotion to " . States::Active);
                break;
        }
    }
    
    public function getLabel():string {
        return $this->label;
    }
}
