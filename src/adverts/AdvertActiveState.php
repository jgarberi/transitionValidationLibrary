<?php

namespace transitionsValidator;

class AdvertActiveState implements AdvertState {

    private $label = States::Active;

    public function promote(callable $ok, callable $error, string $action) {

        switch ($action) {
            case States::Outdated:
                $ok(new AdvertOutdatedState());
                break;
            default:
                $error("{$this->label} state only allows promotion to " . States::Outdated);
                break;
        }
    }
    
    public function getLabel():string {
        return $this->label;
    }
}
