<?php

namespace transitionsValidator;

/**
 * Advert states
 *
 * This enumeration class holds constants for the advert states
 *
 */
final class States {

    /**
     * New/Initial state
     *
     */
    const Initial = 'Initial';

    /**
     * Active state
     *
     */
    const Active = 'Active';

    /**
     * Limited state
     *
     */
    const Limited = 'Limited';

    /**
     * Outdated state
     *
     */
    const Outdated = 'Outdated';

    /**
     * Removed state
     *
     */
    const Removed = 'Removed';
}
