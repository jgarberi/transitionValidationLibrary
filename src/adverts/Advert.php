<?php

namespace transitionsValidator;

class Advert {

    private $title;
    private $client;

    function __construct(string $title_in, string $client_in) {
        $this->client = $client_in;
        $this->title = $title_in;
    }

    public function getClient():string {
        return $this->client;
    }

    public function getTitle():string {
        return $this->title;
    }
}
