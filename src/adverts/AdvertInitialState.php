<?php

namespace transitionsValidator;

class AdvertInitialState implements AdvertState {

    private $label = States::Initial;

    public function promote(callable $ok, callable $error, string $action) {

        switch ($action) {
            case States::Active:
                $ok(new AdvertActiveState());
                break;
            case States::Limited:
                $ok(new AdvertLimitedState());
                break;
            default:
                $error("{$this->label} state only allows promotion either to " . States::Active . " or " . States::Limited);
                break;
        }
    }
    
    public function getLabel():string {
        return $this->label;
    }
}
