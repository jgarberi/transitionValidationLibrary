<?php

namespace transitionsValidator;

class AdvertRemovedState implements AdvertState {

    private $label = States::Removed;

    public function promote(callable $ok, callable $error, string $action) {
        $error("{$this->label} state does not allow any promotions");
    }
    
    public function getLabel():string {
        return $this->label;
    }
}
