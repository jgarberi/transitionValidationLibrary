<?php

namespace transitionsValidator;

class AdvertContext {

    private $advert;
    private $advertState;

    public function __construct(Advert $advert_in) {
        $this->advert = $advert_in;
        $this->advertState = new AdvertInitialState();
    }

    public function promoteAdvert(string $action):self {
        $this->advertState->promote(
            function (AdvertState $next) {
                $this->advertState = $next;
            },
            function (string $error) {
                throw new \RuntimeException($error);
            },
            $action
        );
        return $this;
    }

    public function getAdvert():Advert {
        return $this->advert;
    }

    public function getStateLabel():string {
        return $this->advertState->getLabel();
    }
}