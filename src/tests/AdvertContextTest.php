<?php

use transitionsValidator\States;
use \transitionsValidator\Advert;
use transitionsValidator\AdvertContext;

class AdvertContextTest extends \PHPUnit_Framework_TestCase {

    public function testInitialAdvertContextCreation() {
        $advertContext = $this->returnFullyFormedInitialAdvertContext();
        $this->assertEquals(States::Initial, $advertContext->getStateLabel());
    }

    /**
     * @param string $transitionSteps Steps to be iterated
     * 
     * We test based on the pre established happy paths
     * 
     * @dataProvider providerTestAllowedContextTransitions
     */
    public function testAllowedContextTransitions($transitionSteps) {
        $advertContext = $this->returnFullyFormedInitialAdvertContext();

        foreach ($transitionSteps as $step) {
            $advertContext->promoteAdvert($step);
            $this->assertEquals($step, $advertContext->getStateLabel());
        }
    }

    public function providerTestAllowedContextTransitions() {

        return [
            [
                [
                    // Straight path
                    States::Active,
                    States::Outdated,
                    States::Removed,
                ]
            ],
            [
                [
                    // Deviations
                    States::Limited,
                    States::Active,
                    States::Outdated,
                    States::Active,
                ]
            ]
        ];
    }

    /**
     * @param string $transitionSteps Steps to be iterated
     * 
     * @expectedException Exception 
     * @dataProvider providerTestInvalidContextTransitions
     */
    public function testInvalidContextTransitions($context, $invalidTransitions) {
        
        $collection = [];
        foreach ($invalidTransitions as $step) {
            $collection[] = $step;
            $context->promoteAdvert($step);
        }
    }

    public function providerTestInvalidContextTransitions() {

        return [[
                $this->returnFullyFormedInitialAdvertContext(),
                [ States::Initial, States::Outdated, States::Removed, ]
            ],
            [
                $this->returnFullyFormedLimitedAdvertContext(),
                [ States::Limited, States::Initial, States::Outdated, States::Removed, ]
            ],
            [
                $this->returnFullyFormedActiveAdvertContext(),
                [ States::Active, States::Initial, States::Limited, States::Removed, ]
            ],
            [
                $this->returnFullyFormedOutdatedAdvertContext(),
                [ States::Outdated, States::Active, States::Initial, States::Limited, ]
            ],
            [
                $this->returnFullyFormedRemovedAdvertContext(),
                [ States::Removed, States::Outdated, States::Active, States::Initial, States::Limited, ]
            ]];
    }

    // -------------------------------------------------------------------------

    /**
     * Returns a freshly initialised advertContext
     * @return advertContext
     */
    private function returnFullyFormedInitialAdvertContext() {
        $ad = new Advert("title", "client");
        return new AdvertContext($ad);
    }

    /**
     * Returns an active Context
     * @return advertContext
     */
    private function returnFullyFormedActiveAdvertContext() {
        $advertContext = $this->returnFullyFormedInitialAdvertContext();
        return $advertContext->promoteAdvert(States::Active);
    }

    /**
     * Returns a limited Context
     * @return advertContext
     */
    private function returnFullyFormedLimitedAdvertContext() {
        $advertContext = $this->returnFullyFormedInitialAdvertContext();
        return $advertContext->promoteAdvert(States::Limited);
    }

    /**
     * Returns an outdated Context
     * @return advertContext
     */
    private function returnFullyFormedOutdatedAdvertContext() {
        $advertContext = $this->returnFullyFormedActiveAdvertContext();
        return $advertContext->promoteAdvert(States::Outdated);
    }

    /**
     * Returns a removed Context
     * @return advertContext
     */
    private function returnFullyFormedRemovedAdvertContext() {
        $advertContext = $this->returnFullyFormedOutdatedAdvertContext();
        return $advertContext->promoteAdvert(States::Removed);
    }

}
