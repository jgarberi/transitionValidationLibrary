<?php

class AdvertTest extends \PHPUnit_Framework_TestCase {

    public function testAdvertCreation() {
        $title = "title";
        $client = "client";
        $ad = new \transitionsValidator\Advert($title, $client);

        $this->assertEquals($title, $ad->getTitle());
        $this->assertEquals($client, $ad->getClient());
    }

}
